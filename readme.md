# Sill, SCSS library

This is a toolbox for common css styling that works for modern browsers and IE down to 8.

## Box sizing

Cross browser box sizing
    
    @extend %box-sizing;

## Chisel

### Chiseled Columns

Used to emulate a chiseled border between inline block or table-cell elements

    @include chisel($color);
    @extend %chisel;

### Chiseled Row

Used to emulate a chiseled border between row elements
place on table-row

    @include chisel_rows($color);
    @extend %chisel_row;

### Chiseled Block

Used to emulate a chiseled border between block
place on block

    @include chisel_block($color);
    @extend %chisel_block;

## Clear

Prevents runaround by subsequent content

    @extend %clear;

## Clipping

Good for clipping dropshadow

    @inlcude clip($direction: 'top', $distance: 0);

Clips top flush

    @extend %clip

## Color

### RGBA with IE8 fallback

Used to set rgba and fallback for any rule that takes color parameters
$rule: color, background-color, border-color...
order: ($rule, $r, $g, $b, $a)

    @extend %faded-white { @include rule_rgba(); }; // default
    @extend %faded-black { @include rule_rgba($r: 0, $g: 0, $b: 0); };
    @extend %faded-gray-text { @include rule_rgba('color', 200, 210, 210, 0.6); };

## Layers and Panels

### Layers

Used to position an item relative to the closest relatively-positioned ancestor (position: relative)

    @include layer($top: 0, $right: 0, $bottom: 0, $left: 0, $index: 99, $height: 'auto', $width: 'auto');

Expands to same size as parent

    %layer { @include layer(); }

Overlay - requires rgba jbolt

    .overlay

### Panels

Sticks to given side, set width or height if you want to keep fixed

    %panel-top;
    %panel-right;
    %panel-bottom;
    %panel-left;

## Linear Gradients

### Glass

Used to make a glass-like shine

    @include glass($intensity: 0.29);
    @extend %glass;

### Highlight 

Used to make a top-down highlight

    @include highlight($intensity: 0.70);
    @extend %highlight;

### Shade

Used to make a bottom-up shading

    @include shade($intensity: 0.70);
    @extend %shade;

## Rounded Corners

### All Corners

Used to make rounded corners on all
default is set in main.css.scss

    @include corners($n: 0.25em);
    @extend %corners;

### Specific Corners

Used to make rounded corner on specific corner
$v: top or bottom
$h: left or right
$n: radius default is 3px

    @include corner($v: top,$h: bottom,$n: 3px);

Used to make 3px on a single corner or side's corners

    @extend %corner_b;
    @extend %corner_t;
    @extend %corner_l;
    @extend %corner_r;
    @extend %corner_tl;
    @extend %corner_tr;
    @extend %corner_bl;
    @extend %corner_br;

## Shadows

### Drop Shadow
Used to make a drop shadow, $offset supports southeast or northwest (-n) only. Can 
also be used for inset shadow 

    @include shadow($offest: 0.5em,$blur: 0.5em,$color: #222,$inset : '');
    @extend %shadow-small (0.2em);
    @extend %shadow-large (0.6em);

### Inset Shadow

Used to make a double inset

    @include inset($offest: 1px,$blur: 0, $light: rgba(255,255,255,0.8), $dark: rgba(0,0,0,0.8));
    @extend %shadow-inset;

### Text Shadow

Used to make a drop shadow under text

    @include text_shadow($h: 1px, $v: 1px, $b: 1px, $c: rgba(0,0,0,0.8));
    @extend %text_shadow;

Used to make text look inset

    @include text-inset($h: 1px, $v: 1px, $b: 1px, $dark: rgba(0,0,0,0.8), $light: rgba(255,255,255,0.8));
    @extend %text_inset;
